import 'package:flutter/material.dart';

class InsysColor {
  // background gradient
  static const Color colorPrimary = Color(0xFF576574);
  static const Color colorPrimaryDark = Color(0xFF222f3e);
  static const Color colorAccent = Color(0xFF8395a7);
  static const Color colorText = Color(0xFFFFFFFF);
  static const Color colorPlaceholder = Color(0xFF576574);
  static const Color colorWhite = Color(0xFFFFFFFF);
  static const Color colorDefaultPage = Color(0xFFFFFFFF);

  Map<int, Color> color = {
    50: Color.fromRGBO(136, 14, 79, .1),
    100: Color.fromRGBO(136, 14, 79, .2),
    200: Color.fromRGBO(136, 14, 79, .3),
    300: Color.fromRGBO(136, 14, 79, .4),
    400: Color.fromRGBO(136, 14, 79, .5),
    500: Color.fromRGBO(136, 14, 79, .6),
    600: Color.fromRGBO(136, 14, 79, .7),
    700: Color.fromRGBO(136, 14, 79, .8),
    800: Color.fromRGBO(136, 14, 79, .9),
    900: Color.fromRGBO(136, 14, 79, 1),
  };
}
