import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:insys/src/data/models/global-model.dart';

String handleError(DioError dioError) {
  String errorDescription = "";
  if (dioError is DioError) {
    switch (dioError.type) {
      case DioErrorType.CANCEL:
        errorDescription = "Request to API server was cancelled";
        break;
      case DioErrorType.CONNECT_TIMEOUT:
        errorDescription = "Connection timeout with API server";
        break;
      case DioErrorType.DEFAULT:
        errorDescription =
            "Connection to API server failed due to internet connection";
        break;
      case DioErrorType.RECEIVE_TIMEOUT:
        errorDescription = "Receive timeout in connection with API server";
        break;
      case DioErrorType.RESPONSE:
        if (dioError.response.statusCode == 500) {
          if (dioError.response.data != null) {
            if (dioError.response?.data["Message"] != null) {
              errorDescription = dioError.response?.data["Message"]?.toString();
              if (dioError.response?.data["ExceptionMessage"] != null) {
                errorDescription =
                    "Details: ${dioError.response?.data["ExceptionMessage"]?.toString()}";
              }
            }
          }
        } else {
          try {
            errorDescription = dioError.response.data['message'];
          } catch (e) {
            errorDescription = "${dioError.response.data}";
          }
        }
        break;
      case DioErrorType.SEND_TIMEOUT:
        errorDescription = "Send timeout in connection with API server";
        break;
    }
  } else {
    errorDescription = "Unexpected error occured";
  }
  return errorDescription;
}
