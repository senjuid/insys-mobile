import 'package:insys/src/utils/preferences/preferences_base.dart';

class Prefs {
  static Future<bool> get authenticated => PreferencesHelper.getBool(Const.AUTHENTICATED);

  static Future setAuthenticated(bool value) => PreferencesHelper.setBool(Const.AUTHENTICATED, value);

  static Future<String> get token => PreferencesHelper.getString(Const.TOKEN);

  static Future setToken(String value) => PreferencesHelper.setString(Const.TOKEN, value);

  static Future<String> get empID => PreferencesHelper.getString(Const.EMP_ID);

  static Future setEmpID(String value) => PreferencesHelper.setString(Const.EMP_ID, value);

  static Future<String> get fullName => PreferencesHelper.getString(Const.FULL_NAME);

  static Future setFullName(String value) => PreferencesHelper.setString(Const.FULL_NAME, value);

  static Future<String> get email => PreferencesHelper.getString(Const.EMAIL);

  static Future setEmail(String value) => PreferencesHelper.setString(Const.EMAIL, value);

  static Future<void> clear() async {
    await Future.wait(<Future>[
      setAuthenticated(false),
      setToken(''),
      setEmpID(''),
      setFullName(''),
      setEmail(''),
    ]);
  }
}

class Const {
  // Default preferences
  static const String AUTHENTICATED = 'AUTHENTICATED';
  static const String TOKEN = 'TOKEN';
  static const String EMP_ID = 'EMP_ID';
  static const String FULL_NAME = 'FULL_NAME';
  static const String EMAIL = 'EMAIL';
}
