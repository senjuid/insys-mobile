import 'package:flutter/material.dart';
import 'package:insys/src/widgets/insys-loading.dart';
import 'package:lottie/lottie.dart';

enum InsysButtonState {
  Default,
  Processing,
}

enum InsysButtonType {
  Raised,
  Flat,
  Outline,
}

class InsysButton extends StatefulWidget {
  final Widget defaultWidget;
  final Function onPressed;
  final InsysButtonType type;
  final Color color;
  final Color textColor;
  final double width;
  final double height;
  final double borderRadius;
  final bool animate;
  final bool isLoading;

  InsysButton({
    Key key,
    this.defaultWidget,
    this.onPressed,
    this.type = InsysButtonType.Raised,
    this.color,
    this.textColor,
    this.width = double.infinity,
    this.height = 50.0,
    this.borderRadius = 10.0,
    this.animate = true,
    this.isLoading = false,
  }) : super(key: key);

  @override
  _InsysButtonState createState() => _InsysButtonState();
}

class _InsysButtonState extends State<InsysButton>
    with TickerProviderStateMixin {
  GlobalKey _globalKey = GlobalKey();
  Animation _anim;
  AnimationController _animController;
  Duration _duration = const Duration(milliseconds: 250);
  InsysButtonState _state;
  double _width;
  double _height;
  double _borderRadius;
  Widget progressWidget = InsysLoading();

  @override
  dispose() {
    _animController?.dispose();
    super.dispose();
  }

  @override
  void deactivate() {
    _reset();
    super.deactivate();
  }

  @override
  void initState() {
    _reset();
    super.initState();
  }

  void _reset() {
    _state = InsysButtonState.Default;
    _width = widget.width;
    _height = widget.height;
    _borderRadius = widget.borderRadius;
  }

  @override
  Widget build(BuildContext context) {
    return PhysicalModel(
      color: Colors.transparent,
      borderRadius: BorderRadius.circular(_borderRadius),
      child: SizedBox(
        key: _globalKey,
        height: _height,
        width: _width,
        child: _buildChild(context),
      ),
    );
  }

  Widget _buildChild(BuildContext context) {
    var padding = const EdgeInsets.all(0.0);
    var color = widget.color;
    var textColor = widget.textColor;
    var shape = RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(_borderRadius));

    switch (widget.type) {
      case InsysButtonType.Flat:
        return FlatButton(
          padding: padding,
          color: color,
          textColor: textColor,
          shape: shape,
          child: _buildChildren(context),
          onPressed: _onButtonPressed(),
        );
      case InsysButtonType.Outline:
        return OutlineButton(
          padding: padding,
          color: color,
          textColor: textColor,
          shape: shape,
          child: _buildChildren(context),
          onPressed: _onButtonPressed(),
        );
      default:
        return RaisedButton(
          padding: padding,
          color: color,
          textColor: textColor,
          shape: shape,
          child: _buildChildren(context),
          onPressed: _onButtonPressed(),
        );
    }
  }

  Widget _buildChildren(BuildContext context) {
    Widget ret;
    switch (_state) {
      case InsysButtonState.Default:
        ret = widget.defaultWidget;
        break;
      case InsysButtonState.Processing:
        ret = progressWidget ?? widget.defaultWidget;
        break;
    }
    return ret;
  }

  VoidCallback _onButtonPressed() {
    return widget.onPressed == null
        ? null
        : () async {
            if (_state != InsysButtonState.Default) {
              return;
            }

            // The result of widget.onPressed() will be called as VoidCallback after button status is back to default.
            VoidCallback onDefault;
            if (widget.animate) {
              _toProcessing();
              _forward((status) {
                if (status == AnimationStatus.dismissed) {
                  _toDefault();
                  if (onDefault != null && onDefault is VoidCallback) {
                    onDefault();
                  }
                }
              });
              onDefault = await widget.onPressed();
              _reverse();
            } else {
              _toProcessing();
              onDefault = await widget.onPressed();
              _toDefault();
              if (onDefault != null && onDefault is VoidCallback) {
                onDefault();
              }
            }
          };
  }

  void _toProcessing() {
    setState(() {
      if (widget.isLoading) {
        _state = InsysButtonState.Processing;
      }
    });
  }

  void _toDefault() {
    if (mounted) {
      setState(() {
        _state = InsysButtonState.Default;
      });
    } else {
      _state = InsysButtonState.Default;
    }
  }

  void _forward(AnimationStatusListener stateListener) {
    double initialWidth = _globalKey.currentContext.size.width;
    double initialBorderRadius = widget.borderRadius;
    double targetWidth = _height;
    double targetBorderRadius = _height / 2;

    _animController = AnimationController(duration: _duration, vsync: this);
    _anim = Tween(begin: 0.0, end: 1.0).animate(_animController)
      ..addListener(() {
        setState(() {
          _width = initialWidth - ((initialWidth - targetWidth) * _anim.value);
          _borderRadius = initialBorderRadius -
              ((initialBorderRadius - targetBorderRadius) * _anim.value);
        });
      })
      ..addStatusListener(stateListener);

    _animController.forward();
  }

  void _reverse() {
    _animController.reverse();
  }
}
