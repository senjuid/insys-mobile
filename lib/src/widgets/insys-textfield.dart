import 'package:flutter/material.dart';
import 'package:insys/src/utils/insys-colors.dart';

enum InsysTextFieldType {
  Text,
  Password,
}

class InsysTextField extends StatefulWidget {
  const InsysTextField(
      {this.fieldKey,
      this.maxLength,
      this.hintText,
      this.labelText,
      this.helperText,
      this.onSaved,
      this.validator,
      this.onFieldSubmitted,
      this.enable = true,
      this.textInputType = TextInputType.text,
      this.type = InsysTextFieldType.Text});

  final Key fieldKey;
  final int maxLength;
  final String hintText;
  final String labelText;
  final String helperText;
  final FormFieldSetter<String> onSaved;
  final FormFieldValidator<String> validator;
  final ValueChanged<String> onFieldSubmitted;
  final InsysTextFieldType type;
  final bool enable;
  final TextInputType textInputType;

  @override
  _InsysTextFieldState createState() => _InsysTextFieldState();
}

class _InsysTextFieldState extends State<InsysTextField> {
  bool _obscureText = true;

  @override
  Widget build(BuildContext context) {
    switch (widget.type) {
      case InsysTextFieldType.Password:
        return passwordField(context);
        break;
      default:
        return textField(context);
        break;
    }
  }

  Widget textField(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
        child: TextFormField(
          key: widget.fieldKey,
          maxLength:
              widget.maxLength, // if not provided by the user, then it is 8
          onSaved: widget.onSaved,
          validator: widget.validator,
          enabled: widget.enable,
          onFieldSubmitted: widget.onFieldSubmitted,
          keyboardType: widget.textInputType,
          decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
                borderSide:
                    BorderSide(color: InsysColor.colorPrimaryDark, width: 2.0),
              ),
              filled: true,
              hintStyle: TextStyle(color: InsysColor.colorPlaceholder),
              hintText: widget.hintText,
              labelText: widget.labelText,
              helperText: widget.helperText,
              fillColor: InsysColor.colorText),
        ));
  }

  Widget passwordField(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
        child: TextFormField(
          key: widget.fieldKey,
          obscureText: _obscureText,
          maxLength:
              widget.maxLength, // if not provided by the user, then it is 8
          onSaved: widget.onSaved,
          validator: widget.validator,
          enabled: widget.enable,
          onFieldSubmitted: widget.onFieldSubmitted,
          decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
                borderSide:
                    BorderSide(color: InsysColor.colorPrimaryDark, width: 2.0),
              ),
              filled: true,
              hintStyle: TextStyle(color: InsysColor.colorPlaceholder),
              hintText: widget.hintText,
              labelText: widget.labelText,
              helperText: widget.helperText,
              fillColor: InsysColor.colorText,
              suffixIcon: GestureDetector(
                onTap: () {
                  setState(() {
                    _obscureText = !_obscureText;
                  });
                },
                child: Icon(
                  _obscureText ? Icons.visibility : Icons.visibility_off,
                  color: InsysColor.colorAccent,
                ),
              )),
        ));
  }
}
