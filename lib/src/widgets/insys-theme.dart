import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:insys/src/data/models/themes-model.dart';

class _InsysTheme extends InheritedWidget {
  final InsysThemeState data;

  _InsysTheme({
    this.data,
    Key key,
    @required Widget child,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_InsysTheme oldWidget) {
    return true;
  }
}

class InsysTheme extends StatefulWidget {
  final Widget child;
  final ThemeKeys initialThemeKey;

  const InsysTheme({
    Key key,
    this.initialThemeKey,
    @required this.child,
  }) : super(key: key);

  @override
  InsysThemeState createState() => new InsysThemeState();

  static ThemeData of(BuildContext context) {
    _InsysTheme inherited =
        (context.dependOnInheritedWidgetOfExactType<_InsysTheme>());
    return inherited.data.theme;
  }

  static InsysThemeState instanceOf(BuildContext context) {
    _InsysTheme inherited =
        (context.dependOnInheritedWidgetOfExactType<_InsysTheme>());
    return inherited.data;
  }
}

class InsysThemeState extends State<InsysTheme> {
  ThemeData _theme;

  ThemeData get theme => _theme;

  @override
  void initState() {
    _theme = Themes.getThemeFromKey(widget.initialThemeKey);
    super.initState();
  }

  

  void changeTheme(ThemeKeys themeKey) {
    setState(() {
      _theme = Themes.getThemeFromKey(themeKey);
    });
  }

  @override
  Widget build(BuildContext context) {
    return new _InsysTheme(
      data: this,
      child: widget.child,
    );
  }
}