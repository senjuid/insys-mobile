import 'package:flutter/material.dart';
import 'package:insys/src/data/models/themes-model.dart';

class InsysAppbar extends StatefulWidget implements PreferredSizeWidget {
  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);

  const InsysAppbar(
      {Key key,
      this.title,
      this.leading,
      this.actions,
      this.color,
      this.useTheme = false});

  final Widget title;
  final Widget leading;
  final List<Widget> actions;
  final double elevation = 0.0;
  final Color color;
  final bool useTheme;

  @override
  _InsysAppbarState createState() => _InsysAppbarState();
}

class _InsysAppbarState extends State<InsysAppbar> {
  Widget appBarSelect() {
    if (widget.useTheme) {
      return Theme(
          data: Themes.blueTheme,
          child: PreferredSize(
              child: Builder(
                  builder: (context) => AppBar(
                        title: widget.title,
                        leading: widget.leading,
                        actions: widget.actions,
                        elevation: widget.elevation,
                      )), preferredSize: null,));
    } else {
      return AppBar(
        title: widget.title,
        leading: widget.leading,
        actions: widget.actions,
        elevation: widget.elevation,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return appBarSelect();
  }
}
