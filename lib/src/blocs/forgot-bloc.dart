import 'package:insys/src/data/repository/forgot-repository.dart';

class ForgotBloc {
  final repository = ForgotRepository();

  Future<String> forgot(String email) async {
    try {
      String forgot = await repository.forgot(email);
      return forgot;
    } catch (e) {
      throw e;
    }
  }
}
