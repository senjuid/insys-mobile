import 'package:insys/src/data/models/login-model.dart';
import 'package:insys/src/data/repository/auth-repository.dart';
import 'package:rxdart/rxdart.dart';

class AuthBloc {
  final repository = AuthRepository();
  final PublishSubject _isSessionValid = PublishSubject<bool>();
  Observable<bool> get isSessionValid => _isSessionValid.stream;

  void dispose() {
    _isSessionValid.close();
  }

  void setSession(LoginModel data) async {
    await repository.persistToken(data);
    _isSessionValid.sink.add(true);
  }

  void closeSession() async {
    await repository.deleteToken();
    _isSessionValid.sink.add(false);
  }

  void restoreSession() async {
    var token = await repository.getToken();
    if (token != null && token.length > 0) {
      _isSessionValid.sink.add(true);
    } else {
      _isSessionValid.sink.add(false);
    }
  }

  Future<String> getName() async {
    return await repository.getName();
  }
}

final authBloc = AuthBloc();
