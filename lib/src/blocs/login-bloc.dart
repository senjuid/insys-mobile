import 'package:insys/src/blocs/auth-bloc.dart';
import 'package:insys/src/data/models/login-model.dart';
import 'package:insys/src/data/repository/login-repository.dart';

class LoginBloc {
  final repository = LoginRepository();
  Future<String> login(Login login) async {
    try {
      LoginModel auth = await repository.authenticate(login);
      authBloc.setSession(auth);
      return "";
    } catch (e) {
      throw e;
    }
  }

  Future<String> logout() async {
    try {
      await repository.logout();
      authBloc.closeSession();
      return "";
    } catch (e) {
      throw e;
    }
  }
}
