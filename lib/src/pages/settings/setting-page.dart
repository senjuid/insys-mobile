import 'package:flutter/material.dart';
import 'package:insys/src/data/models/themes-model.dart';
import 'package:insys/src/widgets/insys-theme.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  void _changeTheme(BuildContext buildContext, ThemeKeys key) {
    InsysTheme.instanceOf(buildContext).changeTheme(key);
  }

  @override
  void initState() {
    WidgetsBinding.instance
        .addPostFrameCallback((_) => _changeTheme(context, ThemeKeys.BLUE));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: Text("Homepage"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              RaisedButton(
                onPressed: () {
                  _changeTheme(context, ThemeKeys.LIGHT);
                },
                child: Text("Light!"),
              ),
              RaisedButton(
                onPressed: () {
                  _changeTheme(context, ThemeKeys.DARK);
                },
                child: Text("Dark!"),
              ),
              RaisedButton(
                onPressed: () {
                  _changeTheme(context, ThemeKeys.DARKER);
                },
                child: Text("Darker!"),
              ),
              Divider(
                height: 100,
              ),
              AnimatedContainer(
                duration: Duration(milliseconds: 500),
                color: Theme.of(context).primaryColor,
                width: 100,
                height: 100,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
