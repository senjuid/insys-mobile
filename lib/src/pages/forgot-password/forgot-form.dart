import 'package:flutter/material.dart';
import 'package:insys/src/blocs/forgot-bloc.dart';
import 'package:insys/src/pages/forgot-password/forgot-alert.dart';
import 'package:insys/src/utils/format-date.dart';
import 'package:insys/src/utils/insys-colors.dart';
import 'package:insys/src/widgets/insys-button.dart';
import 'package:insys/src/widgets/insys-textfield.dart';
import 'package:insys/src/widgets/insys_modal.dart';

class ForgotForm extends StatefulWidget {
  @override
  _ForgotFormState createState() => _ForgotFormState();
}

class _ForgotFormState extends State<ForgotForm> {
  final _formKey = GlobalKey<FormState>();
  ForgotBloc bloc = ForgotBloc();
  String frmEmail;
  bool isEnableForm = true;
  bool autoValidation = false;

  void showAlert(String message, bool isSuccess) {
    showInsysModalBottomSheet(
        context: context,
        radius: 30.0,
        color: Colors.white,
        builder: (context) {
          return ForgotAlert(
            message: message,
            isSuccess: isSuccess,
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Builder(
        builder: (context) => Form(
            key: _formKey,
            autovalidate: autoValidation,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  InsysTextField(
                    enable: isEnableForm,
                    validator: validateEmail,
                    hintText: 'Email',
                    onSaved: (val) => {frmEmail = val},
                  ),
                  Container(
                      padding: EdgeInsets.symmetric(vertical: 16.0),
                      child: InsysButton(
                          type: InsysButtonType.Raised,
                          defaultWidget: Text('Send to email'),
                          color: InsysColor.colorPrimary,
                          textColor: InsysColor.colorText,
                          animate: false,
                          width: 114,
                          isLoading: true,
                          onPressed: () async {
                            final form = _formKey.currentState;
                            if (form.validate()) {
                              form.save();
                              try {
                                setState(() {
                                  isEnableForm = false;
                                });
                                await bloc.forgot(frmEmail);
                                setState(() {
                                  isEnableForm = true;
                                });
                                showAlert("", true);
                              } catch (e) {
                                setState(() {
                                  isEnableForm = true;
                                });
                                showAlert("", true);
                              }
                            } else {
                              setState(() {
                                autoValidation = true;
                              });
                            }
                          })),
                ])));
  }
}
