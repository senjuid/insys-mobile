import 'package:flutter/material.dart';
import 'package:insys/src/utils/insys-colors.dart';
import 'package:insys/src/widgets/insys-button.dart';
import 'package:lottie/lottie.dart';

class ForgotAlert extends StatelessWidget {
  final String message;
  final bool isSuccess;

  ForgotAlert({Key key, @required this.message, @required this.isSuccess})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SizedBox(
          width: (isSuccess) ? 100 : 200,
          child: (isSuccess)
              ? Lottie.asset('assets/success-alert.json')
              : Lottie.asset('assets/error-alert.json'),
        ),
        Container(
            padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
            child: Text(
              (isSuccess) ? "Verfication has been sent" : "$message",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: InsysColor.colorPrimary,
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold),
            )),
        Container(
            padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
            child: Text(
              (isSuccess)
                  ? "Check your email to see the verification code"
                  : "",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: InsysColor.colorPrimary,
                  fontSize: 14.0,
                  fontWeight: FontWeight.normal),
            )),
        Container(
            padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
            child: InsysButton(
                type: InsysButtonType.Raised,
                defaultWidget: Text('Close'),
                color: InsysColor.colorPrimary,
                textColor: InsysColor.colorText,
                animate: false,
                onPressed: () async {
                  Navigator.pop(context);
                })),
      ],
    );
  }
}
