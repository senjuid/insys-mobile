import 'package:flutter/material.dart';
import 'package:insys/src/pages/forgot-password/forgot-form.dart';
import 'package:insys/src/utils/insys-colors.dart';
import 'package:insys/src/widgets/insys-appbar.dart';
import 'package:lottie/lottie.dart';

class ForgotPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: InsysColor.colorDefaultPage,
        appBar: InsysAppbar(
            color: InsysColor.colorWhite,
            title: Text("Forgot Password"),
            leading: BackButton(
              onPressed: () {
                Navigator.pop(context);
              },
            )),
        body: Container(
          child: SingleChildScrollView(
            child: Center(
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 30, horizontal: 30),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      width: 200,
                      child:
                          Lottie.asset('assets/forget-password-animation.json'),
                    ),
                    Container(
                        padding: EdgeInsets.only(top: 20.0),
                        child: Text(
                          "Can't remember password?",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: InsysColor.colorPrimary,
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold),
                        )),
                    Container(
                        padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                        child: Text(
                          "Enter your email and we will send a verification code to your email",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: InsysColor.colorPrimary,
                              fontSize: 12.0,
                              fontWeight: FontWeight.normal),
                        )),
                    ForgotForm()
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
