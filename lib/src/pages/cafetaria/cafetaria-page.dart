import 'package:flutter/material.dart';
import 'package:insys/src/utils/insys-colors.dart';
import 'package:insys/src/widgets/insys-appbar.dart';
import 'package:insys/src/widgets/insys-search-bar.dart/search-bar.dart';

class CafetariaPage extends StatefulWidget {
  @override
  _CafetariaPageState createState() => _CafetariaPageState();
}

class _CafetariaPageState extends State<CafetariaPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: InsysColor.colorDefaultPage,
      appBar: InsysAppbar(
          color: InsysColor.colorWhite,
          title: Text("Order Food"),
          leading: BackButton(
            onPressed: () {
              Navigator.pop(context);
            },
          )),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        // child: SearchBar<Post>(
        //     onSearch: null,
        //     onItemFound: (Post post, int index) {
        //       return foodList(context);
        //     },
        //     hintText: "Find food"),
        child: _foodList(context),
      ),
    );
  }

  Widget _foodList(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
            decoration: BoxDecoration(
                color: InsysColor.colorWhite,
                borderRadius: BorderRadius.all(Radius.circular(15)),
                border: Border.all(color: InsysColor.colorWhite),
                boxShadow: [
                  BoxShadow(
                      color: Colors.black12,
                      offset: Offset(1, 1),
                      spreadRadius: 1,
                      blurRadius: 1)
                ]),
            child: Container(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        child: Text(
                      "Food",
                      style: TextStyle(
                          color: InsysColor.colorPrimary,
                          fontSize: 14.0,
                          fontWeight: FontWeight.w400),
                    )),
                    Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 5.0, vertical: 5.0),
                        child: Divider(
                          color: InsysColor.colorAccent,
                          thickness: 0.3,
                        )),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(right: 10.0),
                          height: 50,
                          width: 50,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(
                                    "https://d6ohe4fukqm26.cloudfront.net/sultanjakarta/uploads/81b96d28367ab1707c198e67c300f0c9_crop_920x530.jpg"),
                                fit: BoxFit.fill,
                              ),
                              shape: BoxShape.rectangle,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15))),
                        ),
                        Expanded(
                            flex: 1,
                            child: Column(
                              children: <Widget>[
                                Container(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      "Nasi Goreng Smoke Beef",
                                      style: TextStyle(
                                          color: InsysColor.colorPrimary,
                                          fontSize: 12.0,
                                          fontWeight: FontWeight.w400),
                                    )),
                                SizedBox(height: 15),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                        flex: 1,
                                        child: Text(
                                          "16.000",
                                          style: TextStyle(
                                              color: InsysColor.colorPrimary,
                                              fontSize: 12.0,
                                              fontWeight: FontWeight.w200),
                                        )),
                                    Expanded(
                                        flex: 1,
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: InsysColor.colorWhite,
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(15)),
                                              border: Border.all(
                                                  color: InsysColor.colorWhite),
                                              boxShadow: [
                                                BoxShadow(
                                                    color: Colors.black12,
                                                    offset: Offset(1, 1),
                                                    spreadRadius: 1,
                                                    blurRadius: 1)
                                              ]),
                                          child: Row(
                                            children: <Widget>[
                                              Expanded(
                                                flex: 1,
                                                child: Icon(Icons.remove,
                                                    color:
                                                        InsysColor.colorAccent,
                                                    size: 18.0),
                                              ),
                                              Expanded(
                                                flex: 1,
                                                child: Text("0",
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                      color: InsysColor
                                                          .colorPrimary,
                                                      fontSize: 12.0,
                                                      fontWeight:
                                                          FontWeight.w200,
                                                    )),
                                              ),
                                              Expanded(
                                                flex: 1,
                                                child: Icon(
                                                  Icons.add,
                                                  color: InsysColor.colorAccent,
                                                  size: 18.0
                                                ),
                                              ),
                                            ],
                                          ),
                                        )),
                                  ],
                                )
                              ],
                            ))
                      ],
                    )
                  ],
                ),
              ),
            ))
      ],
    );
  }

  Future<List<Post>> search(String search) async {
    await Future.delayed(Duration(seconds: 2));
    return List.generate(search.length, (int index) {
      return Post(
        "Title : $search $index",
        "Description :$search $index",
      );
    });
  }
}

class Post {
  final String title;
  final String description;

  Post(this.title, this.description);
}
