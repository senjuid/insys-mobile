import 'package:flutter/material.dart';
import 'package:insys/src/blocs/auth-bloc.dart';
import 'package:insys/src/pages/cafetaria/cafetaria-page.dart';
import 'package:insys/src/pages/home/home-detail.dart';
import 'package:insys/src/utils/insys-colors.dart';
import 'package:insys/src/widgets/insys-appbar.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String fullName;

  final snackBar = SnackBar(
    content: Text('Under Development!'),
  );

  void showDetail(context, type) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => HomeDetail(type: type), maintainState: true));
  }

  void showCafetaria(context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => CafetariaPage(), maintainState: true));
  }

  @override
  void initState() {
    showFullName();
    super.initState();
  }

  void showFullName() async {
    final name = await authBloc.getName();
    print(name);
    setState(() {
      fullName = name;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: InsysAppbar(
        useTheme: true,
        title: Padding(
            child: Text(fullName), padding: const EdgeInsets.only(left: 20.0)),
        actions: <Widget>[
          Builder(
            builder: (BuildContext context) {
              return IconButton(
                  icon: Icon(
                    Icons.settings,
                    color: InsysColor.colorWhite,
                  ),
                  onPressed: () => {
                        // Scaffold.of(context).showSnackBar(snackBar)
                        authBloc.closeSession()
                      });
            },
          ),
          Builder(
            builder: (BuildContext context) {
              return IconButton(
                  icon: Icon(
                    Icons.notifications,
                    color: InsysColor.colorWhite,
                  ),
                  onPressed: () =>
                      {Scaffold.of(context).showSnackBar(snackBar)});
            },
          ),
        ],
      ),
      body: Stack(
        children: <Widget>[
          Container(
              height: 100,
              decoration: BoxDecoration(
                  color: InsysColor.colorPrimaryDark,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.elliptical(200, 10),
                    bottomRight: Radius.elliptical(200, 10),
                  ))),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 30),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
                    Widget>[
              Container(
                decoration: BoxDecoration(
                    color: InsysColor.colorWhite,
                    borderRadius: BorderRadius.all(Radius.circular(15)),
                    border: Border.all(color: InsysColor.colorWhite),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black12,
                          offset: Offset(1, 1),
                          spreadRadius: 1,
                          blurRadius: 1)
                    ]),
                margin: EdgeInsets.symmetric(horizontal: 16),
                child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                          flex: 1,
                          child: Builder(builder: (BuildContext context) {
                            return InkWell(
                              splashColor: Colors.blue.withAlpha(30),
                              onTap: () {
                                Scaffold.of(context).showSnackBar(snackBar);
                              },
                              child: Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: 10.0, vertical: 10.0),
                                padding: EdgeInsets.symmetric(
                                    horizontal: 5.0, vertical: 5.0),
                                child: Column(
                                  children: <Widget>[
                                    SizedBox(
                                      child: Image.asset(
                                        "assets/transport.png",
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                    SizedBox(height: 20.0),
                                    Text(
                                      "Transport",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: InsysColor.colorPrimaryDark,
                                          fontSize: 11.0,
                                          fontWeight: FontWeight.w400),
                                    )
                                  ],
                                ),
                              ),
                            );
                          })),
                      Expanded(
                          flex: 1,
                          child: Builder(builder: (BuildContext context) {
                            return InkWell(
                              splashColor: Colors.blue.withAlpha(30),
                              onTap: () {
                                showCafetaria(context);
                              },
                              child: Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: 10.0, vertical: 10.0),
                                padding: EdgeInsets.symmetric(
                                    horizontal: 5.0, vertical: 5.0),
                                child: Column(
                                  children: <Widget>[
                                    SizedBox(
                                      child: Image.asset(
                                        "assets/cafetaria.png",
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                    SizedBox(height: 20.0),
                                    Text(
                                      "Cafetaria",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: InsysColor.colorPrimaryDark,
                                          fontSize: 11.0,
                                          fontWeight: FontWeight.w400),
                                    )
                                  ],
                                ),
                              ),
                            );
                          })),
                      Expanded(
                          flex: 1,
                          child: Builder(builder: (BuildContext context) {
                            return InkWell(
                              splashColor: Colors.blue.withAlpha(30),
                              onTap: () {
                                Scaffold.of(context).showSnackBar(snackBar);
                              },
                              child: Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: 10.0, vertical: 10.0),
                                padding: EdgeInsets.symmetric(
                                    horizontal: 5.0, vertical: 5.0),
                                child: Column(
                                  children: <Widget>[
                                    SizedBox(
                                      child: Image.asset(
                                        "assets/room.png",
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                    SizedBox(height: 20.0),
                                    Text(
                                      "Room",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: InsysColor.colorPrimaryDark,
                                          fontSize: 11.0,
                                          fontWeight: FontWeight.w400),
                                    )
                                  ],
                                ),
                              ),
                            );
                          })),
                    ]),
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.05),
              Container(
                  margin: EdgeInsets.only(left: 20, right: 20),
                  child: Text(
                    "Info and Regulations",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: InsysColor.colorPrimary,
                        fontSize: 18.0,
                        fontWeight: FontWeight.w700),
                  )),
              Container(
                  decoration: BoxDecoration(
                      color: InsysColor.colorWhite,
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      border: Border.all(color: InsysColor.colorWhite),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black12,
                            offset: Offset(1, 1),
                            spreadRadius: 1,
                            blurRadius: 1)
                      ]),
                  margin: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                  child: InkWell(
                      splashColor: Colors.blue.withAlpha(30),
                      onTap: () {
                        showDetail(context, 'cafetaria');
                      },
                      child: Container(
                        child: Padding(
                          padding: EdgeInsets.all(2.0),
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Expanded(
                                      flex: 2,
                                      child: Padding(
                                        padding: EdgeInsets.only(left: 10.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              "Peraturan Meal Overtime",
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                  color:
                                                      InsysColor.colorPrimary,
                                                  fontSize: 12.0,
                                                  fontWeight: FontWeight.w700),
                                            ),
                                            SizedBox(
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.03),
                                            Text(
                                              "Lihat selengkapnya",
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                  color:
                                                      InsysColor.colorPrimary,
                                                  fontSize: 10.0,
                                                  fontWeight: FontWeight.w400),
                                            ),
                                          ],
                                        ),
                                      )),
                                  Expanded(
                                      flex: 3,
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(10),
                                        child: Image.asset(
                                          'assets/chef.png',
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.14,
                                          fit: BoxFit.fill,
                                        ),
                                      ))
                                ],
                              )
                            ],
                          ),
                        ),
                      ))),
              Container(
                  decoration: BoxDecoration(
                      color: InsysColor.colorWhite,
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      border: Border.all(color: InsysColor.colorWhite),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black12,
                            offset: Offset(1, 1),
                            spreadRadius: 1,
                            blurRadius: 1)
                      ]),
                  margin: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                  child: InkWell(
                      splashColor: Colors.blue.withAlpha(30),
                      onTap: () {
                        showDetail(context, 'transport');
                      },
                      child: Container(
                        child: Padding(
                          padding: EdgeInsets.all(2.0),
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Expanded(
                                      flex: 2,
                                      child: Padding(
                                        padding: EdgeInsets.only(left: 10.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              "Peraturan Pengguna Voucher Taxi",
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                  color:
                                                      InsysColor.colorPrimary,
                                                  fontSize: 12.0,
                                                  fontWeight: FontWeight.w700),
                                            ),
                                            SizedBox(
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.03),
                                            Text(
                                              "Lihat selengkapnya",
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                  color:
                                                      InsysColor.colorPrimary,
                                                  fontSize: 10.0,
                                                  fontWeight: FontWeight.w400),
                                            ),
                                          ],
                                        ),
                                      )),
                                  Expanded(
                                      flex: 3,
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(10),
                                        child: Image.asset(
                                          'assets/taxi.png',
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.14,
                                          fit: BoxFit.fill,
                                        ),
                                      ))
                                ],
                              )
                            ],
                          ),
                        ),
                      )))
            ]),
          )
        ],
      ),
    );
  }
}
