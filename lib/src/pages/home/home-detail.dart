import 'package:flutter/material.dart';
import 'package:insys/src/data/models/rules-model.dart';
import 'package:insys/src/utils/insys-colors.dart';
import 'package:insys/src/widgets/insys-appbar.dart';

class HomeDetail extends StatelessWidget {
  final String type;

  HomeDetail({Key key, @required this.type}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var i = 0;
    return Scaffold(
        backgroundColor: InsysColor.colorDefaultPage,
        appBar: InsysAppbar(title: Text("Rules")),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                  decoration: BoxDecoration(
                      color: InsysColor.colorWhite,
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      border: Border.all(color: InsysColor.colorWhite),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black12,
                            offset: Offset(1, 1),
                            spreadRadius: 1,
                            blurRadius: 1)
                      ]),
                  margin: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                  child: InkWell(
                      splashColor: Colors.blue.withAlpha(30),
                      child: Container(
                        child: Padding(
                          padding: EdgeInsets.all(2.0),
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Expanded(
                                      flex: 2,
                                      child: Padding(
                                        padding: EdgeInsets.only(left: 10.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              (type == 'cafetaria')
                                                  ? "Peraturan Meal Overtime"
                                                  : "Peraturan penggunaan voucher taksi dan transport",
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                  color:
                                                      InsysColor.colorPrimary,
                                                  fontSize: 12.0,
                                                  fontWeight: FontWeight.w700),
                                            ),
                                          ],
                                        ),
                                      )),
                                  Expanded(
                                      flex: 3,
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(10),
                                        child: Image.asset(
                                          (type == 'cafetaria')
                                              ? 'assets/chef.png'
                                              : 'assets/taxi.png',
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.14,
                                          fit: BoxFit.fill,
                                        ),
                                      ))
                                ],
                              )
                            ],
                          ),
                        ),
                      ))),
              SizedBox(height: MediaQuery.of(context).size.height * 0.02),
              for (var item in listRules)
                if (item.type == type)
                  Container(
                    padding:
                        EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(flex: 1, child: Text("${i = i + 1}")),
                        Expanded(
                          flex: 15,
                          child: Text(
                            item.rule,
                            style: TextStyle(
                              fontSize: 14,
                              color: InsysColor.colorPrimaryDark,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.02),
            ],
          ),
        ));
  }
}
