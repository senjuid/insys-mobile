import 'package:flutter/material.dart';
import 'package:insys/src/utils/insys-colors.dart';
import 'package:insys/src/widgets/insys-button.dart';
import 'package:lottie/lottie.dart';

class LoginAlert extends StatelessWidget {
  final String message;

  LoginAlert({Key key, @required this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SizedBox(
          width: 200,
          child: Lottie.asset('assets/error-alert.json'),
        ),
        Container(
            padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
            child: Text(
              "$message",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: InsysColor.colorPrimary,
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold),
            )),
        Container(
            padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
            child: InsysButton(
                type: InsysButtonType.Raised,
                defaultWidget: Text('Close'),
                color: InsysColor.colorPrimary,
                textColor: InsysColor.colorText,
                animate: false,
                onPressed: () async {
                  Navigator.pop(context);
                })),
      ],
    );
  }
}
