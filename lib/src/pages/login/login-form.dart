import 'package:flutter/material.dart';
import 'package:insys/src/blocs/login-bloc.dart';
import 'package:insys/src/data/models/login-model.dart';
import 'package:insys/src/pages/login/login-alert.dart';
import 'package:insys/src/utils/insys-colors.dart';
import 'package:insys/src/widgets/insys_modal.dart';
import 'package:insys/src/widgets/insys-button.dart';
import 'package:insys/src/widgets/insys-textfield.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();
  LoginBloc bloc = LoginBloc();
  Login frmLogin = Login();
  bool isEnableForm = true;
  bool autoValidation = false;

  void showAlert(String message) {
    showInsysModalBottomSheet(
        context: context,
        radius: 30.0,
        color: Colors.white,
        builder: (context) {
          return LoginAlert(message: message);
        });
  }

  @override
  Widget build(BuildContext context) {
    return Builder(
        builder: (context) => Form(
            key: _formKey,
            autovalidate: autoValidation,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  InsysTextField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter your user name';
                      } else {
                        return null;
                      }
                    },
                    hintText: 'Username',
                    enable: isEnableForm,
                    onSaved: (val) => {frmLogin.username = val},
                  ),
                  InsysTextField(
                    type: InsysTextFieldType.Password,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter your password';
                      } else {
                        return null;
                      }
                    },
                    hintText: 'Password',
                    enable: isEnableForm,
                    onSaved: (val) => {frmLogin.password = val},
                  ),
                  Container(
                      padding: EdgeInsets.symmetric(vertical: 16.0),
                      child: InsysButton(
                          type: InsysButtonType.Raised,
                          defaultWidget: Text('Login'),
                          color: InsysColor.colorPrimary,
                          textColor: InsysColor.colorText,
                          animate: false,
                          width: 114,
                          isLoading: true,
                          onPressed: () async {
                            final form = _formKey.currentState;
                            if (form.validate()) {
                              form.save();
                              try {
                                setState(() {
                                  isEnableForm = false;
                                });
                                await bloc.login(frmLogin);
                                setState(() {
                                  isEnableForm = true;
                                });
                              } catch (e) {
                                setState(() {
                                  isEnableForm = true;
                                });
                                showAlert(e);
                              }
                            } else {
                              setState(() {
                                autoValidation = true;
                              });
                            }
                          })),
                ])));
  }
}
