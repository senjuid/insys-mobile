import 'package:flutter/material.dart';
import 'package:insys/src/utils/insys-colors.dart';

enum ThemeKeys { LIGHT, BLUE, DARK, DARKER }

class Themes {
  static final ThemeData lightTheme = ThemeData(
    primaryColor: InsysColor.colorWhite,
    brightness: Brightness.light,
  );

  static final ThemeData blueTheme = ThemeData(
    primaryColor: InsysColor.colorPrimaryDark,
    brightness: Brightness.light,
  );

  static final ThemeData darkTheme = ThemeData(
    primaryColor: Colors.grey,
    brightness: Brightness.dark,
  );

  static final ThemeData darkerTheme = ThemeData(
    primaryColor: Colors.black,
    brightness: Brightness.dark,
  );

  static ThemeData getThemeFromKey(ThemeKeys themeKey) {
    switch (themeKey) {
      case ThemeKeys.LIGHT:
        return lightTheme;
      case ThemeKeys.BLUE:
        return blueTheme;
      case ThemeKeys.DARK:
        return darkTheme;
      case ThemeKeys.DARKER:
        return darkerTheme;
      default:
        return lightTheme;
    }
  }
}