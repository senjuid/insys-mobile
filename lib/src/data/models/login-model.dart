class LoginModel {
  String message;
  Data data;

  LoginModel({this.message, this.data});

  LoginModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  User user;
  String token;

  Data({this.user, this.token});

  Data.fromJson(Map<String, dynamic> json) {
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['token'] = this.token;
    return data;
  }
}

class User {
  String empId;
  String firstName;
  String middleName;
  String lastName;
  String userName;
  String emailAddress;
  String empStatus;
  String userRole;
  String mealRole;
  String inventoryRole;
  Null dmsRole;
  Null isInstanceAccess;
  String accessManagementRole;
  String fullname;

  User(
      {this.empId,
        this.firstName,
        this.middleName,
        this.lastName,
        this.userName,
        this.emailAddress,
        this.empStatus,
        this.userRole,
        this.mealRole,
        this.inventoryRole,
        this.dmsRole,
        this.isInstanceAccess,
        this.accessManagementRole,
        this.fullname});

  User.fromJson(Map<String, dynamic> json) {
    empId = json['empId'];
    firstName = json['firstName'];
    middleName = json['middleName'];
    lastName = json['lastName'];
    userName = json['userName'];
    emailAddress = json['emailAddress'];
    empStatus = json['empStatus'];
    userRole = json['userRole'];
    mealRole = json['mealRole'];
    inventoryRole = json['inventoryRole'];
    dmsRole = json['dmsRole'];
    isInstanceAccess = json['isInstanceAccess'];
    accessManagementRole = json['accessManagementRole'];
    fullname = json['fullname'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['empId'] = this.empId;
    data['firstName'] = this.firstName;
    data['middleName'] = this.middleName;
    data['lastName'] = this.lastName;
    data['userName'] = this.userName;
    data['emailAddress'] = this.emailAddress;
    data['empStatus'] = this.empStatus;
    data['userRole'] = this.userRole;
    data['mealRole'] = this.mealRole;
    data['inventoryRole'] = this.inventoryRole;
    data['dmsRole'] = this.dmsRole;
    data['isInstanceAccess'] = this.isInstanceAccess;
    data['accessManagementRole'] = this.accessManagementRole;
    data['fullname'] = this.fullname;
    return data;
  }
}

class Login {
  String username;
  String password;

  Login({this.username, this.password});

  Login.fromJson(Map<String, dynamic> json) {
    username = json['username'];
    password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["username"] = this.username;
    data["password"] = this.password;
    return data;
  }
}