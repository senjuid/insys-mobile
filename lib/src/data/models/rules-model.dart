class RulesModel {
  String type;
  String rule;

  RulesModel({this.type, this.rule});

  RulesModel.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    rule = json['rule'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['rule'] = this.rule;
    return data;
  }
}

List<RulesModel> listRules = [
  RulesModel(
      type: 'cafetaria',
      rule:
          'Untuk setiap pemesanan dapat memesan tambahan 2 Side Dish (detail dapat dilihat di Menu pemesanan).'),
  RulesModel(
      type: 'cafetaria',
      rule: 'Kami baru menambahkan minuman-minuman segar untuk pemesanan.'),
  RulesModel(
      type: 'cafetaria',
      rule:
          'Setiap karyawan yang Overtime dikantor diwajibkan memesan makanan dari cafetaria (tidak boleh dari luar kantor), kecuali karyawan yang lembur di luar kantor (client) dan setiap karyawan yang overtime dan telah memesan meal overtime (tidak dibungkus) wajib makan di kantin.'),
  RulesModel(
      type: 'cafetaria',
      rule:
          'Karyawan yang overtime hanya boleh memesan 2 makanan dan 1 minuman untuk setiap kali request overtime perharinya.'),
  RulesModel(
      type: 'cafetaria',
      rule:
          'Untuk makanan yang dibungkus hanya bisa dilayani 1 menu makanan per employee untuk lembur perharinya.'),
  RulesModel(
      type: 'cafetaria',
      rule:
          'Untuk request Meal Overtime tidak bisa di revisi kecuali ada approval by email dari CTO dan dept head terkait.'),
  RulesModel(
      type: 'transport',
      rule:
          'Karyawan tidak boleh menyimpan voucher taxi, bila tidak digunakan pada hari di mana voucher tersebut diminta, harap dikembalikan kepada HR, Purchasing, atau Project Secretary. Kecuali voucher untuk on duty dan untuk keberangkatan di bawah jam 8 pagi. Biaya voucher yang tidak sesuai dengan tanggal di mana voucher tersebut diminta akan dibebankan kepada karyawan.'),
  RulesModel(
      type: 'transport',
      rule:
          'Pengambilan voucher bisa diminta ke HR, Purchasing, atau Project Secretary, selain itu tidak akan diakui dan akan dibebankan ke karyawan.'),
  RulesModel(
      type: 'transport',
      rule:
          'Dilarang memberikan voucher yang diminta ke karyawan lain, jadi pengisian form dan voucher harus dilakukan oleh orang yang bersangkutan (cth. Jika A yang meminta voucher, maka nama yg tercantum di voucher dan form taxi adalah A), hal ini dilakukan untuk penanggung jawab penggunaan voucher jelas.'),
  RulesModel(
      type: 'transport',
      rule:
          'Pengembalian form taxi harus diberikan langsung ke HR, Purchasing, atau Project Secretary di mana akan ada tanda serah terima form yg harus ditandatangani. Pengembalian ini akan ditunggu paling lama 3 hari setelah pengambilan voucer (kecuali yg on duty lebih dari 3 hari). Pengembalian yg dilakukan lewat dari masanya akan dibebankan ke karyawan yg bersangkutan (nama yg tercantum dalam form taxi dan voucer).'),
  RulesModel(
      type: 'transport',
      rule:
          'Form taxi dan voucher taxi harus diisi dengan lengkap (spt. time start dan time finish, from dan to, no voucer dan no taxi, dll), jika tidak maka biaya akan dibebankan ke karyawan.'),
  RulesModel(
      type: 'transport',
      rule:
          'Diharapkan agar bisa memberitahukan planning penyelesaian dalam hal pengerjaannya di client, sehingga akan mempermudah dalam komunikasi antara Driver dan karyawan.'),
  RulesModel(
      type: 'transport',
      rule:
          'Dilarang menggunakan voucher taxi untuk pulang ke rumah dibawah jam 8 malam (client).'),
  RulesModel(
      type: 'transport',
      rule:
          'Yang sudah melakukan pemesanan transport sebelumnya memang akan menjadi prioritas utama. Akan tetapi jika ada support client yang lebih jauh, maka yang jauh yang akan diutamakan, misalnya A pesan mobil ke Gatsu sehari sebelumnya, sedangkan B pesan mobil ke Tangerang pada hari H, maka yg akan mendapatkan transport adalah B bukan A.'),
];
