import 'package:dio/dio.dart';
import 'package:insys/src/utils/handle-error.dart';
import 'package:insys/src/utils/logging-interceptor.dart';

class ApiConnector {
  Dio _dio;
  String baseUrl = "https://api.greatdayhr.com/insys";

  ApiConnector() {
    if (_dio == null) {
      _dio = createDio();
    }
  }

  Dio createDio() {
    return Dio(BaseOptions(
      connectTimeout: 10 * 1000,
      receiveTimeout: 10 * 1000,
      baseUrl: baseUrl,
    ))
      ..interceptors.add(LoggingInterceptor());
  }

  Future<Response> get(
    String endUrl, {
    Map<String, dynamic> params,
    Options options,
  }) async {
    try {
      return await _dio.get(
        endUrl,
        queryParameters: params,
        options: options,
      );
    } on DioError catch (error) {
      throw handleError(error);
    }
  }

  Future<Response> post(
    String endUrl, {
    data,
    Options options,
  }) async {
    try {
      return await _dio.post(endUrl, data: data, options: options);
    } on DioError catch (error) {
      throw handleError(error);
    }
  }
}
