import 'package:insys/src/data/network/remote/base-api.dart';

class ForgotApi {
  ApiConnector _apiConnector = ApiConnector();

  Future<String> forgot(String email) async {
    try {
      await _apiConnector.post("/auth/forgotPassword", data: {"email": email});
      return "";
    } catch (error) {
      throw error;
    }
  }
}
