import 'package:dio/dio.dart';
import 'package:insys/src/data/models/login-model.dart';
import 'package:insys/src/data/network/remote/base-api.dart';

class LoginApi {
  ApiConnector _apiConnector = ApiConnector();

  Future<LoginModel> login(Login data) async {
    try {
      Response response =
          await _apiConnector.post("/auth/login", data: data.toJson());
      return LoginModel.fromJson(response.data);
    } catch (error) {
      throw error;
    }
  }

  Future<LoginModel> logout() async {
    try {
      Response response = await _apiConnector.post("/auth/logout");
      return LoginModel.fromJson(response.data);
    } catch (error) {
      throw error;
    }
  }
}
