import 'package:insys/src/data/models/login-model.dart';
import 'package:insys/src/utils/preferences/preferences.dart';

class AuthRepository {
  Future<void> deleteToken() async {
    await Prefs.clear();
    return;
  }

  Future<void> persistToken(LoginModel data) async {
    await Prefs.setAuthenticated(true);
    await Prefs.setToken(data.data.token);
    await Prefs.setEmpID(data.data.user.empId);
    await Prefs.setFullName(data.data.user.fullname);
    await Prefs.setEmail(data.data.user.emailAddress);
    return;
  }

  Future<String> getToken() async {
    return await Prefs.token;
  }

  Future<bool> hasAuthenticated() async {
    return await Prefs.authenticated;
  }

  Future<String> getName() async {
    return await Prefs.fullName;
  }
}
