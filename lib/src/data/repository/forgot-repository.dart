import 'package:insys/src/data/network/remote/forgot-api.dart';

class ForgotRepository {
  final api = ForgotApi();

  Future<String> forgot(String email) => api.forgot(email);
}
