import 'package:insys/src/data/models/login-model.dart';
import 'package:insys/src/data/network/remote/login-api.dart';

class LoginRepository {
  final api = LoginApi();

  Future<LoginModel> authenticate(Login login) => api.login(login);

  Future<void> logout() => api.logout();
}
