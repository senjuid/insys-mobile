import 'package:flutter/material.dart';
import 'package:insys/src/blocs/auth-bloc.dart';
import 'package:insys/src/data/models/themes-model.dart';
import 'package:insys/src/pages/home/home-page.dart';
import 'package:insys/src/pages/login/login-page.dart';
import 'package:insys/src/widgets/insys-theme.dart';

void main() => runApp(
      InsysTheme(
        initialThemeKey: ThemeKeys.LIGHT,
        child: MyApp(),
      ),
    );

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    authBloc.restoreSession();
    return MaterialApp(
      title: 'Insys',
      theme: InsysTheme.of(context),
      home: createContent(),
    );
  }

  createContent() {
    return StreamBuilder<bool>(
        stream: authBloc.isSessionValid,
        builder: (context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.hasData && snapshot.data) {
            return HomePage();
          }
          return LoginPage();
        });
  }
}
